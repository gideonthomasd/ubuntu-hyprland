#!/bin/bash

mkdir -p ~/.config/hypr
mkdir -p ~/.config/waybar
mkdir -p ~/.config/wlogout
mkdir -p ~/.config/rofi
mkdir -p ~/.config/neofetch
mkdir -p ~/.config/kitty
mkdir -p ~/.local/share/fonts
mkdir -p ~/wallpaper

cd hypr
cp -r * ~/.config/hypr
cd ..

cd waybar
cp -r * ~/.config/waybar
cd ..

cd wlogout
cp -r * ~/.config/wlogout
cd ..

cd rofi
cp -r * ~/.config/rofi
cd ..

cd neofetch
cp -r * ~/.config/neofetch
cd ..

cd kitty
cp -r * ~/.config/kitty
cd ..

cd fonts
cp -r * ~/.local/share/fonts
cd ..

cd wallpaper
cp -r * ~/wallpaper
cd ..



