#!/bin/bash

./install-scripts/00-dependencies.sh
./install-scripts/00-hypr-pkgs.sh
./install-scripts/hyprlang.sh
./install-scripts/hyprlock.sh
./install-scripts/hyprcursor.sh
./install-scripts/hypridle.sh
./install-scripts/fonts.sh
./install-scripts/swww.sh
./install-scripts/hyprland.sh
./install-scripts/nwg-look.sh
